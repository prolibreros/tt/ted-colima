# [⬅️] Sesión 2 [➡️]

\[TOC\]

## Temas vistos

1.  Elaboración de cornisas
2.  Uso del panel `Propiedades` para alineación de elementos
3.  Adición de texto de muestra
4.  Modificación de estilos para párrafo y carácter predeterminados

## Notas

### Uniformidad en la nomenclatura

El uso de una nomenclatura uniforme en los nombres de páginas maestras o para
los estilos permite a otras personas entender su uso de manera más oportuna.
Esto también te ayudará a entender tu trabajo si lo retomas después de varios
meses o años.

Las recomendaciones para la nomenclatura son las siguientes:

-   Usa las páginas maestras `Normal` para páginas en blanco, ya que [Scribus]
    no permite cambiarles el nombre.
-   Usa palabras que describan los atributos principales de la página maestra o
    del estilo.
-   Usa palabras que describan los aspectos diferenciadores de la página
    maestra o del estilo.

Un ejemplo de **nomenclatura no recomendada** es:

-   Páginas maestras `Normal` tienen numeración.
    -   Inconveniente: el nombre no permite saber que se usa para páginas
        numeradas por lo que la única manera de saberlo es cuando se edita la
        página maestra o cuando se aplica.
-   Páginas maestras `Numerada izquierda` para páginas izquierdas y
    `Numerada izquierda (2)` para páginas derechas:
    -   Inconveniente: no permite saber cuál es la página maestra utilizada
        para las páginas numeradas del lado izquierdo o del derecho.
-   Páginas maestras `Numerada con cornisa` para páginas izquierdas y
    `Numerada con cornisa 2` para páginas derechas:
    -   Inconveniente: no permite saber cuál es para páginas izquierdas o
        derechas.

En continuación con el ejemplo, una **nomenclatura sí recomendada** es:

-   Páginas maestras `Normal` para páginas en blanco.
    -   Conveniencia: por convención se sabe que las páginas `Normal` siempre
        son en blanco, por lo que no es necesario abrir la ventana de edición o
        aplicarla para corroborarlo.
-   Páginas maestras `Numerada izquierda` y `Numerada derecha`:
    -   Conveniencia: cualquiera que use el documento sabrá que son páginas
        maestras con numeración.
-   Páginas maestras `Numerada con cornisa izquierda` y
    `Numerada con cornisa derecha`:
    -   Conveniencia: de nueva cuenta cualquiera que use el documento sabrá
        para qué son estas páginas maestras.

### Del «a ojo de buen cubero» al uso de medidas exactas

Al momento de diseñar los elementos sobre la página es posible hacerlo de dos
maneras:

1.  «A ojo de buen cubero». De manera manual se acomodan los elementos donde se
    desean.
2.  Con medidas exactas. Mediante el panel de `Propiedades` se determinan
    medidas exactas.

Las **ventajas del «a ojo de buen cubero»** son:

-   Menor tiempo requerido para el acomodo de elementos
-   Uso más intuitivo

Las **desventajas del «a ojo de buen cubero»** son:

-   Falta de consistencia en el acomodo de elementos que requieren una
    disposición uniforme
-   Mayor dificultad para realizar cambios a través del documento

Las **ventajas del uso de medidas exactas** son:

-   Mayor consistencia y uniformidad en el acomodo de los elementos
-   Menor dificultad para realizar cambios a través del documento

Las **desventajas del uso de medidas exactas** son:

-   Mayor curva de aprendizaje para acomodar los elementos
-   Uso menos intuitivo que requiere hacer operaciones aritméticas

**Para una maquetación con calidad profesional es preferible el uso de medidas
exactas**. Esto se debe a que con esta modalidad es posible obtener una mayor
uniformidad y consistencia en el documento.

### De la edición directa a la edición mediante estilos

La aproximación de «a ojo de buen cubero» también está presente en la edición
de nuestros textos, por lo que es posible hablar de dos modos de edición:

-   «A ojo de buen cubero». De manera manual se edita el formato; por ejemplo,
    cuando se selecciona un fragmento de texto para cambiarle su tipo de fuente
    y tamaño.
-   Con estilos. Mediante el uso de estilos se estipula el formato cuando estos
    sean aplicados en un fragmento de texto.

Las **ventajas y desventajas del «a ojo de buen cubero»** son las mismas
mencionadas en en la sección anterior:

-   Más rápido e intuitivo
-   Menos consistente y uniforme

Las **ventajas y desventajas del uso de estilos** son similares al uso del
panel de `Propiedades`:

-   Más consistente y uniforme, además de facilitar cambios en todo el
    documento
-   Mayor curva de aprendizaje y menos intuitivo

**Para una maquetación con calidad profesional es preferible el uso de
estilos**. Esto se debe a que con esta modalidad es posible controlar el
formato del texto desde una sola ventana, en lugar de tener que navegar el
documento para hacer los cambios.

### La malla de línea base

La *malla de línea base* son un conjunto de líneas consecutivas de arriba a
abajo en nuestro documento. Si se tiene habilitada su vista,[^1] esta malla
parece como una hoja rayada de una libreta. Aunque se tenga habilitada su
vista, esta malla no aparece en la exportación o impresión del documento, ya
que es solo una referencia visual para su diseño.

La importancia fundamental de esta malla es que permite distribuir el texto en
cada una de sus líneas. Esto permite una alineación uniforme del texto en las
páginas izquierdas y derechas.

![Texto alineado por sí estar anclado a la malla de línea base.]

Cuando el texto no está alineado a la malla se corre el riesgo de tener un
texto desalineado. Esta falta de alineamiento es perceptible cuando se ven las
páginas izquierda y derecha de lado a lado.

![Texto desalineado por no estar anclado a la malla de línea base.]

Aunque es posible tener un texto alineado sin el uso de la malla, esto requiere
tener una relación de medidas entre los tamaños de fuente e interlineados
utilizados en el documento. Así que la manera más sencilla de solucionar este
problema es mediante la alineación del texto a la malla de línea base.

### Tipos de estilos

Los *estilos* son patrones de diseño mediante el formato de atributos que se
gestionan desde un panel y que afectan al texto u otro objeto cuando son
aplicados. En [Scribus] existen los siguientes tipos de estilos:

**Estilo de carácter**. Este estilo afecta el diseño del texto seleccionado.
Los estilos de carácter habituales son para las itálicas, negritas y
versalitas.

**Estilo de párrafo**. Este estilo afecta el diseño de un bloque de texto. Un
bloque es todo el texto que está entre saltos de línea (¶). Los estilos de
párrafo más comunes son los de encabezados, cuerpo de texto y bloques de cita.

**Estilo de tabla**. Este estilo afecta el diseño del texto dentro de una
tabla.

**Estilo de celda**. Este estilo afecta el diseño del texto dentro de una
celda.

**Estilo de línea**. Este estilo estipula los atributos de una línea. Esto
permite tener estilos para líneas sólidas, de punto, de trazo, etcétera.

## Ejercicio

### Tareas preliminares

El siguiente ejercicio parte del [`ejercicio1.sla`] para después cubrir la
tarea de la primera sesión. Por último se trabaja la edición de estilos de
párrafo y de carácter predeterminados.

Para comenzar, haz lo siguiente:

-   Descarga el [`ejercicio1.sla`] o ve a la ubicación donde tienes tu propio
    Ejercicio 1
-   Copia el archivo y renómbralo como `ejercicio2.sla`
-   Abre el archivo `ejercicio2.sla` con [Scribus]

Con esto ya cuentas con lo necesario para comenzar este ejercicio.

### Ajuste de la posición vertical de la numeración

En el Ejercicio 1 se colocó el marco de texto para la numeración dentro de los
márgenes del documento. Si al momento de configurar el documento activaste la
incrustación automática de marcos de texto, esto es un inconveniente ya que por
defecto [Scribus] crea los marcos de texto con la misma anchura y altura del
espacio disponible dentro de los márgenes. Esto quiere decir que la numeración
estará debajo del texto principal, como puede apreciarse en la siguiente imagen
de ejemplo.

![Problema con el marco de texto para la numeración.]

Para solucionar esto:

1.  Ve al panel derecho de `Ordenar páginas`[^2]
2.  Da doble clic sobre la página maestra `Numerada derecha`
3.  Selecciona el marco de texto que contiene la numeración
4.  Ve al panel derecho de `Propiedades`[^3]

![Geometría del marco de texto.]

El panel de `Propiedades` muestra una sección de `Geometría` en donde se pueden
ver las siguientes medidas:

-   `Posición X`: posición horizontal (de izquierda a derecha) del marco de
    texto
-   `Posición Y`: posición vertical (de arriba a abajo) del marco de texto
-   `Anchura`: tamaño horizontal del marco de texto
-   `Altura`: tamaño vertical del marco de texto
-   `Rotación`: rotación del marco de texto en sentido contrario a las
    manecillas del reloj
-   `Punto base`: esquina o centro del marco de texto que se usa como
    referencia para el resto de las medidas

Para arreglar la ubicación de la numeración solo es necesario prestar atención
a dos medidas: `Posición Y` y `Altura`. Lo que se busca es colocar el marco de
texto de la numeración debajo del margen inferior.

Entonces, para saber **con exactitud** cuánto tenemos que aumentar a la
`Posición Y` basta con sumarle la `Altura` del marco más 1 mm adicional. Esto
recorrerá el marco de texto debajo del margen inferior. Para ello:

1.  Modifica la `Posición Y` de `185.000 mm` a `196.000 mm` (`Altura` de
    `10 mm` + `1 mm`)
2.  Repite el mismo procedimiento para la página maestra `Numerada izquierda`

> **TIP**. El uso de las mismas medidas para los márgenes es una mera
> convención adoptada en estos ejercicios y la adición de 1 mm es una cuestión
> de gusto personal. Con el tiempo y la práctica encontrarás tamaños para los
> márgenes que sean de tu agrado, lo importante es tener un uso consistente de
> las medidas que adoptes y ¡no temer a los espacios en blanco!

![Marco de texto para la numeración con la `Posición Y` adecuada.]

### Uso de la malla de línea base para la numeración

Con el ajuste realizado en la sección anterior se tiene un número de página que
está alineado en la parte inferior de su marco de texto. Esto hace que la
numeración se coloque muy abajo del margen, además de que no da garantía de que
se mantenga en la misma posición a lo largo de todas las páginas.

Para tener más certeza, a continuación se alinea la numeración a la malla de
línea base:

1.  Ve al panel derecho de `Ordenar páginas`[^4]
2.  Da doble clic sobre la página maestra `Numerada derecha`
3.  Selecciona el marco de texto que contiene la numeración
4.  Ve al panel derecho de `Propiedades de texto`[^5]
5.  Haz caso omiso a todos los atributos, **excepto**:
    -   Cambia `Interlineado fijo` por `Alinear a la malla de línea base`
    -   Selecciona lo siguiente en los atributos de
        `Columnas y distancias de textos`:
        -   Alineamiento vertical: `Arriba`
6.  Repite el mismo procedimiento para la página maestra `Numerada izquierda`

![Numeración alineada a la malla de línea base.]

Con este cambio ahora se tiene la certeza de que siempre habrá una línea de
separación entre el texto principal y la numeración.

### Creación y alineación de cornisas

A continuación se van a crear las páginas maestras numeradas y con cornisas.
Para ello primero es necesario duplicar las páginas maestras numeradas:

1.  Duplica la página maestra `Numerada derecha` con el nombre
    `Numerada derecha con cornisa`[^6]
2.  Repite el mismo procedimiento con la página maestra `Numerada izquierda`
    para crear la página maestra `Numerada izquierda con cornisa`

![Páginas maestras numeradas y con cornisas creadas por la duplicación de las
páginas maestras numeradas.]

Ahora se duplica el marco de texto de la numeración para crear la cornisa:

1.  Ve al panel derecho de `Ordenar páginas`[^7]
2.  Da doble clic sobre la página maestra `Numerada derecha con cornisa`
3.  Selecciona el marco de texto que contiene la numeración
4.  Copia el marco de texto desde `Editar > Copiar` o con la tecla `Control+C`
5.  Pega el marco de texto desde `Editar > Pegar` o con la tecla `Control+V`
6.  Ve al panel derecho de `Alinear y distribuir`[^8]
7.  Alinea el marco de texto relativo a `Márgenes` y por arriba (segundo botón
    inferior del mismo panel)
8.  Elimina la numeración y en su lugar escribe otro texto; por ejemplo, «Un
    título de una obra»

![Cornisa de la página maestra `Numerada derecha con cornisa`.]

Como puedes observar, falta un paso adicional para alinear de manera correcta
la cornisa. La corrección es mediante la modificación de la `Posición Y` de
este marco de texto:

1.  Selecciona el marco de texto que contiene la cornisa
2.  Ve al panel derecho de `Propiedades`[^9]
3.  Modifica la `Posición Y` de `20.000 mm` a `9.000 mm` (sustracción de la
    `Altura` de `10 mm` y el milímetro de gracia)

![Cornisa en la posición correcta.]

Para terminar:

1.  Repite el mismo procedimiento para la página maestra
    `Numerada izquierda con cornisa` con un texto distinto en la cornisa, por
    ejemplo, «Juanita Pérez Díaz»
2.  Da clic en `Finalizar edición` (botón en la esquina inferior
    izquierda)[^10]
3.  Aplica las páginas maestras recién creadas:[^11]
    -   Para las páginas impares aplica la página maestra
        `Numerada derecha con cornisa` dentro del rango `2 a 10`
    -   Para las páginas pares aplica la página maestra
        `Numerada izquierda con cornisa` dentro del rango `2 a 10`

![Páginas maestras numeradas y con cornisas aplicadas en el documento.]

Con esto habrás terminado *de manera profesional* la tarea del Ejercicio 1.
Como resultado tienes una primera página con solo numeración y el resto de las
páginas con numeración y cornisa. De esta manera se deja la primera página del
documento como el inicio de un capítulo.

Además ya tienes las páginas maestras habituales para cualquier proyecto
editorial:

-   Páginas maestras en blanco, útiles para el [paratexto] (falsa, portadilla,
    legal, colofón, etcétera)
-   Páginas maestras numeradas, útiles para iniciar capítulos
-   Páginas maestras numeradas y con cornisa, útiles para el resto de las
    páginas

Algunos proyectos requieren páginas maestras adicionales para los siguientes
casos:

1.  Cuando se quiere una numeración distinta a la arábiga, por ejemplo, en
    números romanos. Esta numeración puede ser habitual en las páginas del
    [paratexto], como en la tabla de contenidos, un prólogo o un epílogo.[^12]
2.  Cuando se requiere una cornisa distinta en cada capítulo. Esto es habitual
    cuando se trata de una antología donde cada autor y su texto tienden a
    estar en las cornisas izquierda y derecha respectivamente, o cuando es una
    obra extensa en donde algunas cornisas se utilizan para el nombre del
    capítulo o sección.[^13]
3.  Cuando se pretenden páginas maestras con diseño distinto. Esto es habitual
    cuando en una publicación se quiere diferenciar cada sección mediante el
    diseño.

Esta complejidad se soluciona mediante un uso extenso y dedicado de los
principios vistos aquí sobre la creación, modificación y aplicación de páginas
maestras. Un ejemplo de este uso puede encontrarse en el archivo de [Scribus]
del libro [*WikiEducación. Prácticas y experiencias educativas*] de [Wikimedia
México].

### Adición de texto de muestra

A continuación se comienza a trabajar con la modificación y usos de estilos.
Para ello es necesario contar con algún texto que permita visualizar en vivo
los cambios de estilos.

Para hacer enfoque en el diseño se va a prescindir del uso del texto definitivo
y en su lugar se va a llenar el documento con un texto de muestra. Entonces, a
continuación:

1.  Selecciona un marco de texto del contenido principal (los marcos de texto
    más grandes de tu documento)
2.  Haz clic derecho sobre el marco seleccionado y ve a
    `Contenido > Texto de prueba…`

Al texto de prueba también se le conoce como [*lorem ipsum*] y desde la ventana
flotante abierta por [Scribus] haces lo siguiente:

1.  Selecciona `Lorem Ipsum estándar`
2.  Elige `99` párrafos
3.  Da clic en `Aceptar`

![Ventana flotante al dar clic en `Contenido > Texto de prueba…`.]

Con esto tienes un montón de texto en seudolatín que te permite trabajar en el
diseño de estilos sin requerir el texto definitivo. Así puedes concentrarte en
el diseño y más adelante aplicarás los estilos en el texto definitivo.

Con la vista de cadena de texto habilitada,[^14] es posible observar que los
marcos de texto principales del documento están encadenados. Por este motivo el
texto de muestra se despliega en todas las páginas que forman parte de esta
cadena.

> **TIP**. En un calendario editorial apretado, comenzar el diseño de una
> publicación con texto de muestra permite solapar y comprimir los procesos
> editoriales en donde quien edita trabaja con el contenido del texto al mismo
> tiempo que quien diseña lo hace con su forma.

![Texto de muestra después de dar clic en `Aceptar`.]

### Modificación de estilo de carácter predeterminado

El estilo de carácter predeterminado es el formato de fuente que se aplica al
texto de manera automática y predeterminada. Por lo general es deseable que
este estilo sea para el formato de texto más común en el documento, ya que es
el que no requiere ningún atributo de formato adicional. Para aplicar otros
estilos, que bien pueden usar otra fuente, es necesario que el usuario los cree
y aplique en el texto.

Para modificar el estilo de carácter predeterminado:

1.  Da clic en `Editar > Estilos…` para abrir la ventana flotante del
    `Gestor de estilo`
2.  Da doble clic en `Estilo de carácter predeterminado`
3.  Modifica los siguientes atributos de la pestaña `Propiedades`:
    -   Familia: el de tu preferencia,[^15] para este ejercicio es
        [`Montserrat`]
    -   Estilo: el de tu preferencia, para este ejercicio es `Medium`
    -   Tamaño de tipo: `11 pt`
    -   Ancho de espacio predeterminado: `85%`
    -   Idioma: `Spanish (Generic)`
4.  Da clic en `Aplicar` o `<< Terminado` y cierra la ventana flotante
5.  Activa la vista previa desde `Ver > Vista previa > Modo de vista previa` o
    con la tecla `Control+Alt+P`
6.  Desactiva la vista previa cuando termines de ver los cambios

![Configuración del estilo de carácter predeterminado.]

> **TIP**. Uno de los trucos para tener una formación mejor distribuida del
> texto es la realización de pequeñas modificaciones en su tamaño y espacios
> predeterminados. De manera habitual se usa el tamaño de 13 o 14 pt para
> libros infantiles, 11 pt para el resto de los libros y 10 pt para
> publicaciones periódicas. Un espacio predeterminado menor ayuda a evitar
> espacios amplios, además de que permite tener una mayor cantidad de texto por
> página y, por lo tanto, disminuye la cantidad de páginas y de papel. Estas
> medidas están abiertas a interpretación, así como dependen de la fuente
> utilizada, ¡siente la libertad de experimentar!

![Texto de muestra con la modificación en el estilo de carácter
predeterminado.]

### Modificación de estilo de párrafo predeterminado

El estilo de párrafo predeterminado es el formato que se aplica a los bloques
de texto de manera automática y predeterminada. Como el estilo de carácter
predeterminado, es deseable que este estilo sea para el formato de texto más
común en el documento.

Para modificar el estilo de párrafo predeterminado:

1.  Da clic en `Editar > Estilos…` para abrir la ventana flotante de
    `Gestor de estilo`
2.  Da doble clic en `Estilo de párrafo predeterminado`
3.  Modifica los siguientes atributos de la pestaña `Propiedades`:
    -   Alineación: `Justificar texto`
    -   Modo de interlineado: `Alinear a malla de la línea base`
    -   Espacio de anchura mínimo: `85%`
    -   Sangría de primera línea: `5 mm`
4.  Modifica el siguiente atributo de la pestaña `Estilo de carácter`:
    -   Basada en: `Default Character Style`
5.  Da clic en `Aplicar` o `<< Terminado` y cierra la ventana flotante
6.  Activa la vista previa desde `Ver > Vista previa > Modo de vista previa` o
    con la tecla `Control+Alt+P`
7.  Desactiva la vista previa cuando termines de ver los cambios

![Configuración del estilo de párrafo predeterminado.]

Aunque el estilo de párrafo predeterminado permite modificar el formato de
fuente, es conveniente que sea gestionado a partir del estilo de carácter
predeterminado. Por ello en la pestaña de `Estilo de carácter` se eligió que
esté basado en `Default Character Style`, el nombre en inglés del estilo de
carácter predeterminado.

> **TIP**. De nueva cuenta uno de los trucos para tener una formación mejor
> distribuida del texto es la realización de pequeñas modificaciones en sus
> espacios predeterminados, como el uso de espacio de anchura mínimo al 85%.

![Texto de muestra con la modificación en el estilo de párrafo predeterminado.]

### Separación silábica

El uso de alineación justificada tiende a acarrear continuos inconvenientes con
la distribución de los espacios. Aunque las configuraciones anteriores están
pensadas para evitarlos, en más de una ocasión te encontrarás con líneas que
tienen mucho espacio, como se aprecia en la siguiente imagen.

![Espacios anchos en el texto.]

Una manera de lidiar con este problema es con la habilitación de la división
silábica:

1.  Selecciona un marco de texto del contenido principal
2.  Activa la separación silábica desde `Extras > Dividir en sílabas`[^16]

![Texto con separación silábica.]

Como puedes observar, la separación silábica es útil para arreglar casi todos
los espacios sobrantes entre las líneas de texto.

> **TIP**. Cuando tengas columnas muy estrechas con alineación justificada y
> los espacios sobrantes no pueden ser eliminados con ninguna de las técnicas
> vistas hasta ahora, en ese caso lo más conveniente es optar por una
> alineación a la izquierda y sin separación silábica. Aunque en libros esta
> alineación no es frecuente, en publicaciones periódicas es la manera en como
> tiende a resolverse este problema.

![Texto de muestra con la separación silábica activada.]

### Creación de estilo de párrafo

Un documento contiene encabezados, los cuales pueden ser nombres de capítulos,
secciones o subsecciones. Por lo general estos encabezados forman parte de una
jerarquía donde, por ejemplo, los capítulos contienen secciones y las
subsecciones forman parte de una sección. Aunque no hay consensos universales
sobre la nomenclatura, el orden y la cantidad de encabezados, la mayoría de las
publicaciones no exceden los seis niveles de encabezados.

El siguiente paso de este ejercicio es la creación de un estilo de párrafo para
los encabezados de primera jerarquía. Para comenzar:

1.  Da clic en `Editar > Estilos…` para abrir la ventana flotante de
    `Gestor de estilo`
2.  Da clic en `Nuevo > Estilo de párrafo`
3.  Nombra el estilo como `Encabezados`
4.  Ve a la pestaña `Propiedades`
5.  Modifica los siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Default Paragraph Style`
    -   Alineación: `Centrar texto`
    -   Espacio por debajo: `33 pt`
    -   Sangría de primera línea: `0 mm`
6.  Ve a la pestaña `Estilo de carácter`
7.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Estilo: el de tu preferencia, para este ejercicio es `Light`
    -   Tamaño de tipo: `24 pt`
8.  Da clic en `Aplicar`
9.  Da clic en `Nuevo > Estilo de párrafo`
10. Nombra el estilo como `Encabezado 1`
11. Ve a la pestaña `Propiedades`
12. Modifica el siguiente atributo de la pestaña `Propiedades`:
    -   Basada en: `Encabezados`
13. Da clic en `Aplicar` o `<< Terminado` y cierra la ventana flotante

![Configuración del estilo de párrafo `Encabezado 1`.]

El estilo de párrafo `Encabezados` sirve para agrupar el resto de los estilos.
El estilo `Encabezado 1` tiene el mismo formato del estilo `Encabezados`.
Cuando necesites crear los estilos de `Encabezado 2` o de `Encabezado 3`, usa
como base el estilo `Encabezados` para que todos se conserven en el mismo
grupo. A partir de ahí puedes indicar el formato específico para cada nivel de
encabezado. Por ejemplo, podrías indicar que `Encabezado 2` esté alineado a la
izquierda con un tamaño de fuente de 18 pt.

### Aplicación de estilo de párrafo

Para terminar con este ejercicio se aplica el estilo `Encabezado 1` a la
primera oración del texto de muestra. Para ello:

1.  Ve a la página 1 del documento
2.  Añade un salto de línea después de la primera oración del texto de muestra
    para separarlo del resto
3.  Elimina el punto de la primera oración

![Primera oración separada del resto del texto de muestra.]

A continuación se aplica el estilo de párrafo:

1.  Da doble clic sobre el marco de texto que contiene la primera oración
2.  Coloca el cursor en algún carácter de la primera oración
3.  Ve al panel derecho de `Propiedades de texto`[^17]
4.  Haz caso omiso a todos los atributos, **excepto**:
    -   Cambia `[Estilo de párrafo predeterminado]` por `Encabezado 1`

![Panel de `Propiedades de texto` con el cursos sobre la primera oración.]

Para terminar, elimina la separación silábica del encabezado:

1.  Selecciona todo el texto del encabezado
2.  Desactiva la separación silábica desde
    `Extras > Eliminar división silábica`

> **TIP**. Para encabezados no es recomendado usar la separación silábica ya
> que los hace menos legibles y estéticamente menos agradables.

![Primera oración con el estilo `Encabezado 1` aplicado.]

Enhorabuena, haz completado el segundo ejercicio de este taller. Si tienes
dudas sobre el resultado final, puedes consultar el archivo de [este
ejercicio].

## Tarea

1.  Crea un nuevo estilo `Primer párrafo` basado en el estilo de párrafo
    predeterminado
2.  Aplica el estilo `Primer párrafo` al párrafo inmediatamente después del
    `Encabezado 1`

## Enlaces de interés

-   [Archivo de este ejercicio][este ejercicio]
-   [Archivo del Ejercicio 1][`ejercicio1.sla`]
-   [Archivo del libro *WikiEducación. Prácticas y experiencias
    educativas*][*WikiEducación. Prácticas y experiencias educativas*]
-   [Repositorio de la fuente Montserrat][`Montserrat`]
-   [Catálogo de Font Library]
-   [Catálogo de Google Fonts]
-   [Artículo de Wikipedia sobre el paratexto][paratexto]
-   [Artículo en Wikipedia sobre *lorem ipsum*][*lorem ipsum*]

[^1]: En [Scribus] se habilita la vista de la malla de línea base desde
    `Ver > Marcos de texto > Ver rejilla de línea base`.

[^2]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Ordenar páginas`.

[^3]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Propiedades`.

[^4]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Ordenar páginas`.

[^5]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Content Properties`. El panel se encuentra nombrado como
    `Propiedades de texto`, una pequeña inconsistencia en la interfaz de
    [Scribus].

[^6]: En el Ejercicio 1, sección «[Duplicación de una página maestra]», se
    explica de manera más extensa cómo duplicar páginas maestras.

[^7]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Ordenar páginas`.

[^8]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Alinear y distribuir`. En el Ejercicio 1, sección «[Creación y
    alineación de un marco de texto]», se explica de manera más extensa cómo
    alinear elementos con este panel.

[^9]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Propiedades`.

[^10]: En el Ejercicio 1, sección «[Arreglo de un error de visualización]», se
    explica cómo arreglar el potencial problema en la visualización del
    documento después de hacer clic en `Finalizar edición`.

[^11]: En el Ejercicio 1, sección «[Aplicación de páginas maestras]», se
    explica de manera más extensa cómo aplicar páginas maestras.

[^12]: En InDesign se puede evitar esta duplicación de páginas maestras
    mediante la creación de secciones. En [Scribus] existe una función similar
    llamada `Marcas`; por desgracia, aún no está del todo implementada, por lo
    que la vía para obtener el mismo resultado es mediante la duplicación.

[^13]: En InDesign se puede evitar esta duplicación de páginas maestras
    mediante la creación de secciones. En [Scribus] existe una función similar
    llamada `Marcas`; por desgracia, aún no está del todo implementada, por lo
    que la vía para obtener el mismo resultado es mediante la duplicación.

[^14]: La vista de las cadenas de texto pueden habilitarse o deshabilitarse
    desde `Ver > Marcos de texto > Ver cadena de texto`.

[^15]: [Font Library][Catálogo de Font Library] y [Google
    Fonts][Catálogo de Google Fonts] son dos catálogos con fuentes abiertas y
    libres que puedes usar sin costo alguno.

[^16]: Si no funciona, intenta de nuevo. Si después de varios intentos sigue
    sin funcionar, prueba con usar otra variante del idioma español de las
    propiedades del estilo de carácter predeterminado (se supone que
    `Spanish (Generic)` no debería presentar este problema). Si sigue sin
    funcionar, es probablemente que estés usando una versión de [Scribus] que
    no lo permite.

[^17]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Content Properties`. El panel se encuentra nombrado como
    `Propiedades de texto`, una pequeña inconsistencia en la interfaz de
    [Scribus].

  [⬅️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md
  [➡️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion3.md
  [Scribus]: https://www.scribus.net
  [Texto alineado por sí estar anclado a la malla de línea base.]: ../imagenes/sesion2_01.jpg
  [Texto desalineado por no estar anclado a la malla de línea base.]: ../imagenes/sesion2_02.jpg
  [`ejercicio1.sla`]: https://gitlab.com/prolibreros/tt/ted-colima/-/raw/main/ejercicios/ejercicio1.sla?ref_type=heads&inline=false
  [Problema con el marco de texto para la numeración.]: ../imagenes/sesion2_03.jpg
  [Geometría del marco de texto.]: ../imagenes/sesion2_04.jpg
  [Marco de texto para la numeración con la `Posición Y` adecuada.]: ../imagenes/sesion2_05.jpg
  [Numeración alineada a la malla de línea base.]: ../imagenes/sesion2_06.jpg
  [Páginas maestras numeradas y con cornisas creadas por la duplicación de las páginas maestras numeradas.]:
    ../imagenes/sesion2_07.jpg
  [Cornisa de la página maestra `Numerada derecha con cornisa`.]: ../imagenes/sesion2_08.jpg
  [Cornisa en la posición correcta.]: ../imagenes/sesion2_09.jpg
  [Páginas maestras numeradas y con cornisas aplicadas en el documento.]: ../imagenes/sesion2_10.jpg
  [paratexto]: https://es.wikipedia.org/wiki/Paratexto
  [*WikiEducación. Prácticas y experiencias educativas*]: https://gitlab.com/prolibreros/cc/wikieducacion2022/-/raw/no-masters/src/print.sla?ref_type=heads&inline=false
  [Wikimedia México]: https://mx.wikimedia.org
  [*lorem ipsum*]: https://es.wikipedia.org/wiki/Lorem_ipsum
  [Ventana flotante al dar clic en `Contenido > Texto de prueba…`.]: ../imagenes/sesion2_11.jpg
  [Texto de muestra después de dar clic en `Aceptar`.]: ../imagenes/sesion2_12.jpg
  [`Montserrat`]: https://github.com/JulietaUla/Montserrat
  [Configuración del estilo de carácter predeterminado.]: ../imagenes/sesion2_13.jpg
  [Texto de muestra con la modificación en el estilo de carácter predeterminado.]:
    ../imagenes/sesion2_14.jpg
  [Configuración del estilo de párrafo predeterminado.]: ../imagenes/sesion2_15.jpg
  [Texto de muestra con la modificación en el estilo de párrafo predeterminado.]:
    ../imagenes/sesion2_16.jpg
  [Espacios anchos en el texto.]: ../imagenes/sesion2_17.jpg
  [Texto con separación silábica.]: ../imagenes/sesion2_18.jpg
  [Texto de muestra con la separación silábica activada.]: ../imagenes/sesion2_19.jpg
  [Configuración del estilo de párrafo `Encabezado 1`.]: ../imagenes/sesion2_20.jpg
  [Primera oración separada del resto del texto de muestra.]: ../imagenes/sesion2_21.jpg
  [Panel de `Propiedades de texto` con el cursos sobre la primera oración.]: ../imagenes/sesion2_22.jpg
  [Primera oración con el estilo `Encabezado 1` aplicado.]: ../imagenes/sesion2_23.jpg
  [este ejercicio]: https://gitlab.com/prolibreros/tt/ted-colima/-/raw/main/ejercicios/ejercicio2.sla?ref_type=heads&inline=false
  [Catálogo de Font Library]: https://fontlibrary.org
  [Catálogo de Google Fonts]: https://fonts.google.com
  [Duplicación de una página maestra]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md?ref_type=heads#duplicaci%C3%B3n-de-una-p%C3%A1gina-maestra
  [Creación y alineación de un marco de texto]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md?ref_type=heads#creaci%C3%B3n-y-alineaci%C3%B3n-de-un-marco-de-texto
  [Arreglo de un error de visualización]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md?ref_type=heads#arreglo-de-un-error-de-visualizaci%C3%B3n
  [Aplicación de páginas maestras]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md?ref_type=heads#aplicaci%C3%B3n-de-p%C3%A1ginas-maestras
