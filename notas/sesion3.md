# [⬅️] Sesión 3 [➡️]

\[TOC\]

## Temas vistos

1.  Creación, modificación y aplicación de estilos para párrafo

## Notas

### Ciclo para el diseño y uso de estilos

El diseño y uso de estilos de párrafo y carácter implica un ciclo con las
siguientes fases:

**Creación**. El primer paso para un nuevo estilo es su creación. Esto lo haces
al ir a `Editar > Estilos…` para abrir la ventana flotante del
`Gestor de estilo` y das clic en `Nuevo` o en `Clonar`.

**Modificación**. Cuando el estilo se crea, es posible hacer las modificaciones
correspondientes en las pestañas que se abren del lado derecho. Estas pestañas
también se abren si desde la ventana flotante del `Gestor de estilo` haces
doble clic en el estilo que deseas modificar.

**Aplicación**. Cuando el estilo se crea, puedes aplicarlo en cualquier texto
desde el panel derecho de `Propiedades de texto`. Este panel también se abre
desde `Ventanas > Content Properties`.

Según el ritmo de trabajo y de tus preferencias habrá momentos en los que
apliques un estilo antes de modificarlo o viceversa. El ciclo para el diseño y
uso de estilos no es completamente unidireccional: «creación → modificación →
aplicación». En su lugar, de manera parcial es bidireccional: «creación →
modificación ↔ aplicación». Por ello la relevancia de este ciclo no es el orden
de sus operaciones, sino que siempre harás estas operaciones en conjunto sin
importar cómo quieras diseñar y usar los estilos.

## Ejercicio

### Tareas preliminares

El siguiente ejercicio parte del [`ejercicio2.sla`] para después cubrir la
tarea de la segunda sesión. Por último se diseñan y usan casi todos los estilos
de párrafo comunes en cualquier proyecto editorial.

![Estilos antes y después de este ejercicio.]

Para comenzar, haz lo siguiente:

-   Descarga el [`ejercicio2.sla`] o ve a la ubicación donde tienes tu propio
    Ejercicio 2
-   Copia el archivo y renómbralo como `ejercicio3.sla`
-   Abre el archivo `ejercicio3.sla` con [Scribus]

Con esto ya cuentas con lo necesario para comenzar este ejercicio.

### Estilo para primer párrafo

La tarea del Ejercicio 2 consiste en la creación y aplicación del estilo
`Primer párrafo`. Para ello:

1.  Da clic en `Editar > Estilos…` para abrir la ventana flotante de
    `Gestor de estilo`
2.  Da clic en `Nuevo > Estilo de párrafo`
3.  Nombra el estilo como `Primer párrafo`
4.  Ve a la pestaña `Propiedades`
5.  Elige las siguientes opciones de la pestaña `Propiedades`:
    -   Basada en: `Default Paragraph Style`
    -   Sangría de primera línea: `0 mm`
6.  Da clic en `Aplicar` o `<< Terminado` y cierra la ventana flotante
7.  Aplica el estilo sobre el primer párrafo del documento[^1]

![Configuración y aplicación del estilo `Primer párrafo`.]

El estilo para el **primer párrafo**, valga la redundancia, se usa para el
primer párrafo del documento. En español también es común aplicar este estilo
después de cada encabezado, bloque de cita o lista. Entonces, piensa al *primer
párrafo* como el estilo que se aplica al párrafo que sigue después de cualquier
otro párrafo que no tiene el estilo del párrafo predeterminado.

Los pasos hechos en esta sección es la manera concreta en como llevas a cabo el
ciclo de creación, modificación y aplicación de estilos. Otra variación de este
ciclo es realizar los pasos 4 y 5 al final, de esta manera aplicas el estilo
antes de modificarlo. En las próximas secciones siente la libertad de ejecutar
este ciclo como te parezca más conveniente.

> **TIP**. Siempre guarda el documento después de hacer algo importante, como
> es la creación y aplicación de un estilo. ¡No pierdas tu trabajo por un error
> de [Scribus]!

### Ajuste al nombre del estilo para los encabezados

El estilo `Encabezados` se renombra ahora como `Encabezados (grupo)`. Para
ello:

1.  Da clic en `Editar > Estilos…` para abrir la ventana flotante de
    `Gestor de estilo`
2.  Da doble clic en el estilo `Encabezados`
3.  Renombra el estilo como `Encabezados (grupo)`
4.  Da clic en `Aplicar` o `<< Terminado` y cierra la ventana flotante

Todos los estilos pueden usarse desde el panel de `Propiedades de texto`. Sin
embargo, los estilos para agrupar otros, como `Encabezados (grupo)`, no están
pensados para aplicarse directamente en el texto, sino para agrupar atributos
en común de ciertos estilos que sí se piensan aplicar en el documento, como
`Encabezado 1`, `Encabezado 2`, etcétera. Por ello, se adoptan las siguientes
convenciones:

-   Se usa el sufijo `(grupo)` para denotar estilos que están diseñados
    exclusivamente para agrupar otros estilos y que no deben usarse de manera
    directa en el texto.
-   Se usa el término **estilo grupal** para designar de manera genérica a los
    estilos creados para agrupar otros.

Con estas convenciones, es fácil distinguir cuáles estilos se tratan de un
*estilo grupal* al ver la lista de estilos desde el panel de
`Propiedades de texto`.

![Lista de estilos desde el panel de `Propiedades de texto`.]

Por último, otra convención oportuna para saber cuál estilo no predeterminado
está aplicado sobre un texto de muestra, es la escritura del nombre del estilo
en el texto. Esto permite que otras personas conozcan los estilos aplicados en
un documento de muestra. Para esto:

1.  Ve al inicio del documento
2.  Da doble clic sobre el marco de texto que contiene el título del documento
3.  Reemplaza el texto de muestra por `Encabezado 1`

> **TIP**. La escritura de los nombres de los estilos en un texto de muestra
> puede agilizar la corrección y visto bueno del diseño en dos sentidos:
> permite enviar un PDF a otras personas para su evaluación; permite que estas
> personas indiquen correcciones al diseño mediante la mención explícita del
> nombre de estilo que desean cambiar.

![Título del documento con el texto reemplazado.]

### Estilo para encabezado 2

En esta y las siguientes secciones se terminan de elaborar los seis niveles de
encabezados.

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Encabezado 2` con los siguientes atributos de la
    pestaña `Propiedades`:
    -   Basada en: `Encabezados (grupo)`
    -   Alineación: `Alinear texto a la izquierda`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Modifica el siguiente atributo de la pestaña `Estilo de carácter`:
    -   Tamaño de tipo: `16 pt`
3.  Guarda y aplica el estilo en el documento.

El encabezado 2 tiene el mismo peso de fuente que el encabezado 1. El
razonamiento es que sea fácil de distinguir una continuidad entre estos dos
niveles de encabezados, al mismo tiempo que la alineación y los tamaños de
fuente y de espaciado permitan distinguir que no se trata del mismo nivel de
encabezado.

![Configuración y aplicación del estilo `Encabezado 2`.]

### Estilo para encabezado 3

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Encabezado 3` con los siguientes atributos de la
    pestaña `Propiedades`:
    -   Basada en: `Encabezados (grupo)`
    -   Alineación: `Alinear texto a la izquierda`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Estilo: `Medium`
    -   Tamaño de tipo: `13 pt`
3.  Guarda y aplica el estilo en el documento.

El encabezado 3 tiene un peso de fuente distinto al del encabezado 2. El
razonamiento es que la alineación y los tamaños de fuente y de espacio no
permiten una suficiente distinción entre los dos encabezados. Al momento de
elaborar distintos encabezados, se busca cierta continuidad pero también cierta
discontinuidad que permita distinguir que estos estilos son encabezados pero
que al mismo tiempo se trata de diferentes encabezados.

Las alineaciones, los espacios y los estilo de fuente permiten jugar con esta
tensión entre la continuidad y la discontinuidad. Esto quiere decir que las
pautas de diseño de los encabezados en este ejercicio no son una regla general,
sino un criterio personal. En el diseño de tus publicaciones, siente la
libertad de explorar otros criterios para la continuidad y discontinuidad
necesaria en los encabezados.

![Configuración y aplicación del estilo `Encabezado 3`.]

### Estilo para encabezado 4

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Encabezado 4` con los siguientes atributos de la
    pestaña `Propiedades`:
    -   Basada en: `Encabezados (grupo)`
    -   Alineación: `Alinear texto a la izquierda`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Estilo: `Bold`
    -   Tamaño de tipo: `11 pt`
3.  Guarda y aplica el estilo en el documento.

El encabezado 4 se distingue del encabezado 3 en su tamaño y peso de fuente. El
razonamiento a partir de ahora es que los encabezados 4 al 6 serían
infrecuentes y, con el fin de mantener cierta continuidad entre estos, solo se
distinguirán por su peso.

![Configuración y aplicación del estilo `Encabezado 4`.]

### Estilo para encabezado 5

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Encabezado 5` con los siguientes atributos de la
    pestaña `Propiedades`:
    -   Basada en: `Encabezados (grupo)`
    -   Alineación: `Alinear texto a la izquierda`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Estilo: `Bold Italic`
    -   Tamaño de tipo: `11 pt`
3.  Guarda y aplica el estilo en el documento.

Como se mencionó en la sección anterior, el encabezado 5 se distingue del
encabezado 4 solo por el peso de fuente. Sin duda este criterio puede no ser
suficiente para que se distingue la jerarquía entre los encabezados 4 al 6. No
obstante, muchas veces este problema empieza por el sobreuso de encabezados en
un texto. Si tu publicación hace un amplio uso de encabezados 4 al 6, la
recomendación es que les des más criterios distintivos, como puede ser la
alineación o más atributos a sus caracteres.

![Configuración y aplicación del estilo `Encabezado 5`.]

### Estilo para encabezado 6

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Encabezado 6` con los siguientes atributos de la
    pestaña `Propiedades`:
    -   Basada en: `Encabezados (grupo)`
    -   Alineación: `Alinear texto a la izquierda`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Estilo: `Italic`
    -   Tamaño de tipo: `11 pt`
3.  Guarda y aplica el estilo en el documento.

Con esto se terminan de elaborar los estilos para los encabezados. Vale la pena
recordar que el diseño de los encabezados de este ejercicio suponen que los
encabezados 4 al 6 son infrecuentes. Si no es el caso para tu publicación,
siente la libertad de explorar más criterios para la continuidad y
discontinuidad entre los encabezados.

![Configuración y aplicación del estilo `Encabezado 6`.]

### Estilo para cornisa y numeración de página

En más de una ocasión durante el diseño de una publicación decidirás hacer
cambios que consideras que armonizan mejor con la concepción integral del
diseño que estás elaborando. Este es el caso para las cornisas y la numeración
de página de este ejercicio. Aunque su diseño no es defectuoso, los siguientes
cambios se consideran que van más en línea con la manera en como está quedando
el diseño de este ejercicio.

Entonces, de manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Cornisa y número de página (al centro)` con los
    siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Default Paragraph Style`
    -   Alineación: `Centrar texto`
    -   Sangría de primera línea: `0 mm`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Tamaño de tipo: `9 pt`
    -   Formato: `Capitales` (quinto botón del formateo básico)
3.  Guarda el estilo en el documento.

![Configuración del estilo `Cornisa y número de página (al centro)`.]

Ahora se crea un estilo similar pero con alineación a la izquierda:

1.  Crea el estilo de párrafo `Cornisa y número de página (a la izquierda)` con
    los siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Cornisa y número de página (a la izquierda)`
    -   Alineación: `Alinear texto a la derecha`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Cornisa y número de página (a la izquierda)`.]

A continuación se crea un estilo con alineación a la derecha:

1.  Crea el estilo de párrafo `Cornisa y número de página (a la derecha)` con
    los siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Cornisa y número de página (a la derecha)`
    -   Alineación: `Alinear texto a la derecha`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Cornisa y número de página (a la derecha)`.]

Para aplicar los estilos se siguen los siguientes criterios:

1.  Aplica el estilo `Cornisa y número de página (al centro)` a la numeración
    de las páginas maestras `Numerada izquierda` y `Numerada derecha`
2.  Aplica el estilo `Cornisa y número de página (a la izquierda)` a la cornisa
    de la página maestra `Numerada izquierda con cornisa`
3.  Aplica el estilo `Cornisa y número de página (a la derecha)` a la cornisa
    de la página maestra `Numerada derecha con cornisa`

Por último, con el fin de tener un diseño más compacto, haz lo siguiente:

1.  Elimina la numeración de páginas de las páginas maestras con cornisas
2.  Añade la numeración al extremo exterior de cada cornisa con una separación
    de tres espacios

![Aplicación de los estilos para las cornisas y numeraciones de página.]

### Estilo para bloque de cita

Existen ciertos estilos, como para los bloques de cita, que requieren
diferentes variaciones según se trate de un solo bloque o de bloques sucesivos
del mismo estilo. Por ejemplo, cuando es un solo bloque de cita, de manera
convencional se requiere que haya espacios arriba y abajo del bloque. Sin
embargo, cuando son varios bloques de cita, es necesario que solo el primer y
el último bloque tengan espacios. Por este criterio, es menester cuatro
variaciones para el bloque de cita:

1.  Una variación con espacios arriba y abajo para los bloque de cita únicos.
2.  Una variación con espacio arriba para el inicio de bloques de cita
    consecutivos.
3.  Una variación sin espacios para los bloques de cita consecutivos que no
    están al inicio o al final.
4.  Una variación con espacio abajo para el fin de bloques de cita
    consecutivos.

En esta sección se elaboran estas cuatro variaciones para los bloques de cita.

De manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Bloque de cita (sin espacios)` con los
    siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Default Paragraph Style`
    -   Sangría izquierda: `5 mm`
2.  Modifica los siguientes atributos de la pestaña `Estilo de carácter`:
    -   Tamaño de tipo: `9 pt`
3.  Guarda el estilo en el documento.

![Configuración del estilo `Bloque de cita (sin espacios)`.]

Ahora se crea un estilo similar pero con espacio abajo:

1.  Crea el estilo de párrafo `Bloque de cita (con espacio abajo)` con los
    siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Bloque de cita (sin espacios)`
    -   Espacio por debajo: `11 pt`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Bloque de cita (con espacio abajo)`.]

A continuación se crea un estilo con espacio arriba:

1.  Crea el estilo de párrafo `Bloque de cita (con espacio arriba)` con los
    siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Bloque de cita (sin espacios)`
    -   Espacio por encima: `11 pt`
    -   Sangría de primera línea: `0 mm`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Bloque de cita (con espacio arriba)`.]

Por último se crea un estilo con espacios arriba y abajo:

1.  Crea el estilo de párrafo `Bloque de cita (con espacios)` con los
    siguientes atributos de la pestaña `Propiedades`:
    -   Basada en: `Bloque de cita (sin espacios)`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
    -   Sangría de primera línea: `0 mm`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Bloque de cita (con espacios)`.]

El estilo de `Bloque de cita (sin espacios)` es el estilo que indica los
atributos para cualquier bloque de cita. El resto de los estilos son
variaciones en su espaciado. Esto quiere decir que para modificar todos los
bloques de cita solo es necesario modificar el estilo
`Bloque de cita (sin espacios)`, lo que simplifica y facilita su gestión.

![Aplicación de los estilos para los bloques de cita.]

### Estilo para párrafo francés

Otro de los estilos que requiere cuatro variaciones para su espaciado es el
[párrafo francés]. Este tipo de párrafo es común para la bibliografía, donde
todo el párrafo está recorrido *excepto* la primera línea. Para lograr este
efecto es necesaria una configuración sencilla pero poco intuitiva, por lo que
vale la pena que aprendas cómo hacerlo.

![Aplicación de los estilos para los párrafos franceses.]

Entonces, de manera similar a la sección anterior:

1.  Crea el estilo de párrafo `Francés (sin espacios)` con los siguientes
    atributos de la pestaña `Propiedades`:
    -   Basada en: `Default Paragraph Style`
    -   Alineación: `Alinear texto a la izquierda`
    -   Sangría izquierda: `5 mm`
    -   Sangría de primera línea: `-5 mm`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Francés (sin espacios)`.]

El aspecto poco intuitivo del párrafo francés es que la sangría de primera
línea es un número negativo. Todo el párrafo se recorre y se resta la misma
cantidad para regresar la primera línea a la posición inicial. Si [Scribus] no
te permite indicar una sangría de primera línea negativa es porque primero
tienes que indicar la medida de la sangría izquierda.

Ahora se crea un estilo similar pero con espacio abajo:

1.  Crea el estilo de párrafo `Francés (con espacio abajo)` con los siguientes
    atributos de la pestaña `Propiedades`:
    -   Basada en: `Francés (sin espacios)`
    -   Espacio por debajo: `11 pt`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Francés (con espacio abajo)`.]

A continuación se crea un estilo con espacio arriba:

1.  Crea el estilo de párrafo `Francés (con espacio arriba)` con los siguientes
    atributos de la pestaña `Propiedades`:
    -   Basada en: `Francés (sin espacios)`
    -   Espacio por encima: `11 pt`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Francés (con espacio arriba)`.]

Por último se crea un estilo con espacios arriba y abajo:

1.  Crea el estilo de párrafo `Francés (con espacios)` con los siguientes
    atributos de la pestaña `Propiedades`:
    -   Basada en: `Francés (sin espacios)`
    -   Espacio por encima: `11 pt`
    -   Espacio por debajo: `11 pt`
2.  Guarda el estilo en el documento.

![Configuración del estilo `Francés (con espacios)`.]

El estilo de `Francés (sin espacios)` es el estilo que indica los atributos
para cualquier párrafo francés, lo que simplifica y facilita su gestión.

Tu proyecto editorial con seguridad necesitará más estilos con diferentes
alineaciones, espaciados o resaltes. Por ejemplo, tu publicación quizá requiera
epígrafes, párrafos centrados o recuadros (bloques con fondo). Los principios
de diseño y agrupación de estilos para estos bloques son los mismos que hiciste
para los bloques de cita y los párrafos franceses: usa cuatro variaciones en su
espaciado y elige uno de los estilos para controlar el resto de las
variaciones.

Enhorabuena, haz completado el tercer ejercicio de este taller. Si tienes dudas
sobre el resultado final, puedes consultar el archivo de [este ejercicio].

## Tarea

1.  Asegúrate de crear y aplicar todos los estilos vistos en este ejercicio

## Enlaces de interés

-   [¿Qué es un párrafo francés?][párrafo francés]

[^1]: En el Ejercicio 2, sección «[Aplicación de estilo de párrafo]», se
    explica de manera más extensa cómo aplicar estilos.

  [⬅️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion2.md
  [➡️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion4.md
  [`ejercicio2.sla`]: https://gitlab.com/prolibreros/tt/ted-colima/-/raw/main/ejercicios/ejercicio2.sla?ref_type=heads&inline=false
  [Estilos antes y después de este ejercicio.]: ../imagenes/sesion3_01.jpg
  [Scribus]: https://www.scribus.net
  [Configuración y aplicación del estilo `Primer párrafo`.]: ../imagenes/sesion3_02.jpg
  [Lista de estilos desde el panel de `Propiedades de texto`.]: ../imagenes/sesion3_03.jpg
  [Título del documento con el texto reemplazado.]: ../imagenes/sesion3_04.jpg
  [Configuración y aplicación del estilo `Encabezado 2`.]: ../imagenes/sesion3_05.jpg
  [Configuración y aplicación del estilo `Encabezado 3`.]: ../imagenes/sesion3_06.jpg
  [Configuración y aplicación del estilo `Encabezado 4`.]: ../imagenes/sesion3_07.jpg
  [Configuración y aplicación del estilo `Encabezado 5`.]: ../imagenes/sesion3_08.jpg
  [Configuración y aplicación del estilo `Encabezado 6`.]: ../imagenes/sesion3_09.jpg
  [Configuración del estilo `Cornisa y número de página (al centro)`.]: ../imagenes/sesion3_10.jpg
  [Configuración del estilo `Cornisa y número de página (a la izquierda)`.]: ../imagenes/sesion3_11.jpg
  [Configuración del estilo `Cornisa y número de página (a la derecha)`.]: ../imagenes/sesion3_12.jpg
  [Aplicación de los estilos para las cornisas y numeraciones de página.]: ../imagenes/sesion3_13.jpg
  [Configuración del estilo `Bloque de cita (sin espacios)`.]: ../imagenes/sesion3_14.jpg
  [Configuración del estilo `Bloque de cita (con espacio abajo)`.]: ../imagenes/sesion3_15.jpg
  [Configuración del estilo `Bloque de cita (con espacio arriba)`.]: ../imagenes/sesion3_16.jpg
  [Configuración del estilo `Bloque de cita (con espacios)`.]: ../imagenes/sesion3_17.jpg
  [Aplicación de los estilos para los bloques de cita.]: ../imagenes/sesion3_18.jpg
  [párrafo francés]: http://glosariografico.com/parrafo_frances
  [Aplicación de los estilos para los párrafos franceses.]: ../imagenes/sesion3_19.jpg
  [Configuración del estilo `Francés (sin espacios)`.]: ../imagenes/sesion3_20.jpg
  [Configuración del estilo `Francés (con espacio abajo)`.]: ../imagenes/sesion3_21.jpg
  [Configuración del estilo `Francés (con espacio arriba)`.]: ../imagenes/sesion3_22.jpg
  [Configuración del estilo `Francés (con espacios)`.]: ../imagenes/sesion3_23.jpg
  [este ejercicio]: https://gitlab.com/prolibreros/tt/ted-colima/-/raw/main/ejercicios/ejercicio3.sla?ref_type=heads&inline=false
  [Aplicación de estilo de párrafo]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion2.md#aplicaci%C3%B3n-de-estilo-de-p%C3%A1rrafo
