# Sesión 1 [➡️]

\[TOC\]

## Temas vistos

1.  ¿Qué es [Scribus]? Consulta de materiales de apoyo
2.  Exploración de la interfaz de [Scribus]
3.  Configuración de un nuevo documento de [Scribus]
4.  Elaboración y aplicación de páginas maestras

## Notas

### De lo general a lo particular

Para la elaboración de publicaciones con medios digitales existen tres grandes
familias de programas:

1.  Programas de edición de vectores como Corel, Illustrator e [Inkscape]
2.  Programas de composición tipográfica como [TeX]
3.  Programas de maquetación ([*desktop publishing*] en inglés) como
    QuarkXPress, InDesign o [Scribus]

Las **ventajas de los editores de vectores** para la elaboración de
publicaciones son:

-   Programas conocidos por diseñadores e impresores
-   Programas que permiten de manera rápida la elaboración de carteles y
    folletos

Las **desventajas de los editores de vectores** son:

-   Manejo manual del diseño que se presta a faltas de uniformidad
-   Dificultad en cuidar la edición cuando la publicación es extensa o
    periódica

Las **ventajas de los compositores tipográficos** son:

-   Gran calidad tipográfica
-   Vastas oportunidades de automatización

Las **desventajas de los compositores tipográficos** son:

-   Larga curva de aprendizaje
-   Difícil de gestionar cuando la publicación requiere «mucho diseño»

Las **ventajas de los maquetadores** son:

-   Fácil de aprender puesto que es una interfaz pensada para personas dedicas
    al diseño gráfico
-   Simplificación del cuidado editorial para publicaciones extensas o
    periódicas

Las **desventajas de los maquetadores** son:

-   Menores posibilidades de automatización
-   Difícil de producir diversos formatos además de un PDF

**Para las necesidades editoriales de la Subsecretaría de Cultura los programas
de maquetación son los más adecuados**. Esto se debe a que se requiere la
producción de publicaciones extensas o periódicas con programas pensados para
el diseño.

Entre las opciones disponibles para la composición tipográfica se tienen
QuarkXPress, InDesign y [Scribus]. ¿Cuál elegir?

-   Se descarta QuarkXPress porque es *software* de pago y poco popular.
-   Se descarta InDesign porque es *software* de pago que requiere muchos
    recursos computacionales.

Entonces, se elige [Scribus] por estas características:

-   Es [*software* libre][]: no requiere pago.
-   Es ligero: pesa menos de 100 MB.
-   Es multiplataforma: funciona en Windows, macOS y Linux.
-   Es hecho por una comunidad: carece de intereses comerciales.
-   Usa pocos recursos computacionales: no requiere computadoras de alta gama.

### Recursos sobre Scribus

Existen los siguientes recursos para aprender a usar [Scribus][]:

-   [Wiki de Scribus]
-   [Tutorial de Scribus]
-   [Manual de Scribus]
-   [Videos en YouTube]

### Ejemplos hechos con Scribus

-   En el sitio de [Scribus] hay [una sección en inglés] sobre publicaciones
    hechas con este programa.
-   Cuando formé parte del Círculo Ometeotl, la revista Colimotl fue mi primera
    publicación hecha con [Scribus]; ve número [1], [2], [3] y [4].
-   Como parte de [Programando LIBREros] la última vez que usé [Scribus] fue en
    enero 2023 para el libro [*WikiEducación. Prácticas y experiencias
    educativas*] de [Wikimedia México].

### Dos conceptos fundamentales del *desktop publishing*

Los maquetadores de texto facilitan la elaboración de publicaciones extensas o
periódicas gracias a dos conceptos fundamentales:

**Páginas maestras**. Las *páginas maestras* son «plantillas» que permiten
gestionar el diseño común de varias páginas en una sola página denominada
«maestra». Por ejemplo, es posible tener páginas maestras para mostrar de
manera uniforme y repetitiva los números de página, las cornisas u otros
elementos. Gracias a esto es posible mantener la uniformidad en el documento al
mismo tiempo que permite editarlos de manera sencilla.

**Estilos de párrafo y de carácter**. Los *estilos* también permiten gestionar
un diseño en común, pero en este caso para párrafos y caracteres. Por ejemplo,
cabe la posibilidad de tener estilos de párrafo para los encabezados, cuerpo de
texto o bloques de cita; así como es posible tener estilos de carácter para
cursivas, negritas o versalitas. Gracias a esto se mantiene la uniformidad en
el documento al mismo tiempo que permite su sencilla modificación.

Entre más uniformidad y simplificación se tenga en el manejo de la estructura
del contenido, como en páginas, párrafos y caracteres, existe menos margen para
descuidos en la edición y, por lo tanto, aumenta las posibilidades de mejorar
la calidad de una publicación.

## Ejercicio

### Creación de un documento

Para crear un documento con [Scribus], haz lo siguiente:

1.  Abre [Scribus]
2.  Modifica los siguientes atributos de la ventana `Nuevo documento`:[^1]
    -   Disposición del documento: `Páginas enfrentadas`
    -   Tamaño: `Personalizado`
    -   Orientación: `Vertical`
    -   Anchura: `140.000 mm`
    -   Altura: `215.000 mm`
    -   Unidad por defecto: `Milímetros (mm)`
    -   Guías de los márgenes:
        -   Esquema prefijado: `Ninguno`
        -   Interior, Exterior, Superior e Inferior: `20.000 mm`
    -   Sangrías: ninguna (`0.000 mm`)
    -   Opciones:
        -   Números de páginas: `10`
        -   La primera página es: `Página derecha`
        -   Marcos de textos automáticos: sí
        -   Columnas: `1`
        -   Espaciado: `4.000 mm`
        -   Mostrar configuración del documento tras su creación: no
3.  Da clic en `Aceptar`
4.  Guarda el documento como `ejercicio1.sla` desde `Archivo > Guardar`

![Ventana de nuevo documento.]

Las medidas del documento son convencionales, por lo que tienes la libertad de
utilizar las medidas que te parezcan más adecuadas. Sin embargo, vale la pena
hacer las siguientes recomendaciones:

-   Usa páginas enfrentadas cuando quieres un PDF impreso en *offset* y usa
    páginas simples cuando es para impresión digital.
-   Usa medidas sin decimales para que más adelante sea más sencillo acomodar y
    mover los elementos.
-   Usa marcos de textos automáticos para que no tengas que crear marcos cada
    vez que insertas una nueva página.

> **TIP**. ¡No le temas a los espacios en blanco! Parte de un «buen» diseño
> editorial implica el uso de blancos que permiten una mejor distribución del
> contenido. Si tu página es igual o mayor a un tamaño carta, usa un esquema
> prefijado que permite una mejor distribución del espacio. De lo contrario,
> usa márgenes con medidas que no sean menores a 20.000 mm.

![Nuevo documento después de dar clic en `Aceptar`.]

### Cambios en la visualización del documento

Por defecto [Scribus] nos muestra el documento como se ve en la imagen
anterior.

Sin embargo, vale la pena poder ver los siguientes elementos:

-   Rejilla de línea base. Esta rejilla nos permitirá mantener el interlineado
    del texto de manera uniforme en el documento.
-   Cadena de texto. Esta cadena permite ver la manera en como los marcos de
    texto están encadenados, para así poder saber la distribución del texto a
    través de las páginas.
-   Caracteres de control. Esta opción permite ver los caracteres no
    imprimibles como espacios y saltos de línea, lo cual a su vez nos facilita
    la detección de dobles espacios o caracteres indeseados.

Para poder activar estas opciones, haz lo siguiente:

1.  Selecciona `Ver` del menú principal que está en la parte superior
2.  Selecciona la opción `Marcos de texto`
3.  Habilita todas las opciones:
    -   `Ver rejilla de línea base`
    -   `Ver columnas en marco de texto`
    -   `Ver cadena de texto`
    -   `Ver caracteres de control`

> **TIP**. A veces las cosas no podrían funcionar como se esperaban y por algún
> motivo el programa puede congelarse. Aunque [Scribus] guarda un archivo de
> respaldo, es ampliamente recomendable que guardes el documento desde
> `Archivo > Guardar` cada vez que vayas a hacer algo importante o hayas
> terminado de hacerlo.

![Nuevo documento después de habilitar todas las opciones de
`Marcos de texto`.]

### Creación de una página maestra

Una de las primeras cosas por hacer son las páginas maestras del documento. Por
lo general una publicación tiene al menos dos páginas maestras:

1.  Una página maestra numerada que permite tener páginas numeradas de manera
    automática
2.  Una página maestra en blanco para tener páginas que no están numeradas

Cuando se trabaja con un documento con páginas enfrentadas las páginas maestras
se duplican porque se requieren páginas maestras para el lado derecho y el
izquierdo.

Para la creación de páginas maestras, haz lo siguiente:

1.  Ve al panel derecho de `Ordenar páginas`[^2]

Este panel está dividido en tres secciones:

1.  Páginas maestras disponibles
2.  Páginas del documento
3.  Distribución del documento

![Panel de `Ordenar páginas`.]

La sección que nos interesa es la de páginas maestras disponibles, ahí se
encuentran las páginas maestras del documento. Por defecto [Scribus] nos da
páginas maestras en blanco denominadas `Normal`. Si el documento es de páginas
simples, solo mostrará una página maestra `Normal`; de lo contrario, nos
mostrará una página maestra `Normal derecha` y otra `Normal izquierda`.

Para continuar con la creación de páginas maestras:

1.  Da doble clic sobre alguna de las páginas maestras disponibles

![Ventana de edición de páginas maestras.]

Al hacer doble clic sobre una página maestra [Scribus] nos abrirá su ventana de
edición. Es fácil saber que se está en este modo de edición de páginas maestras
si se observan cuatro elementos:

1.  En la esquina inferior izquierda hay un botón de `Finalizar edición`.
2.  Cuando se está en el modo de edición de páginas maestras, el nombre del
    panel `Ordenar páginas` cambia a `Gestionar páginas maestras`.
3.  En el panel de `Gestionar páginas maestras` solo hay una lista de páginas
    maestras disponibles.
4.  En la parte inferior del mismo panel hay un botón de
    `Volver al modo normal de página`.

Además, el modo de edición muestra cuatro botones en la parte superior del
panel de `Gestionar páginas maestras`:

1.  `Añadir una nueva página maestra` (primer botón)
2.  `Importar páginas maestras de otro documento`
3.  `Duplicar la página maestra elegida`
4.  `Borrar la página maestra elegida`

![Cuatro botones para gestionar páginas maestras.]

Para terminar la creación de una página maestra:

1.  Da clic en el botón `Añadir una nueva página maestra`
2.  Ingresa la siguiente información en la ventana flotante:
    -   Nombre: `Numerada derecha`
    -   Disposición: `Página derecha`
3.  Da clic en `Aceptar`

![Ventana flotante al dar clic en `Añadir una nueva página maestra`.]

Con esto ya se tiene una nueva página maestra a la cual a continuación se le
añadirá un marco de texto para su numeración.

### Creación y alineación de un marco de texto

Para poder añadir el número de página a la página maestra `Numerada derecha` es
necesario crear un marco de texto:

1.  Da clic en el botón `Marco de texto` o da clic en
    `Insertar > Marco de texto`
2.  Da clic en el documento para crear un marco de texto

![Antes y después de crear un `Marco de texto`.]

Con el marco de texto creado **y seleccionado**, a continuación se le da el
tamaño apropiado:

1.  Ve al panel derecho de `Propiedades`[^3]
2.  Haz caso omiso a todas las opciones, **excepto**:
    -   Anchura: `100.000 mm`
    -   Altura: `10.000 mm`

![Panel de `Propiedades` con el marco de texto seleccionado.]

Con esto ya tenemos un marco de texto con las medidas adecuadas. A continuación
es necesario alinearlo en el lugar adecuado:

1.  Ve al panel derecho de `Alinear y distribuir`[^4]
2.  Modifica los siguientes atributos de la pestaña `Alinear`:
    -   Relativo a: `Márgenes`
    -   Guía seleccionada: `Nada seleccionado`
    -   Alinear lados por: `Mover`
3.  Da clic en el botón `Alinear por la izquierda` (segundo botón superior del
    mismo panel)
4.  Da clic en el botón `Alinear por abajo` (cuarto botón inferior)

![Panel de `Alinear y distribuir` con el marco de texto seleccionado.]

Con esto ya se cuenta con un marco de texto en la página maestra
`Numerada derecha`. Ahora es necesario añadirle el número de página.

> **TIP**. Aunque es posible usar solo el ratón para llevar a cabo la creación
> y la alineación de marcos de texto, el uso de los paneles de `Propiedades` y
> de `Alinear y distribuir` permiten un trabajo con mayor exactitud, al mismo
> tiempo que permite explorar la configuración de otras propiedades que pueden
> ser idóneas para otros casos. Sí es cierto, el uso de estos paneles requieren
> de mayor tiempo para su dominio. Sin embargo, al mediano plazo esta inversión
> de tiempo es recompensada porque permite la manipulación de *cualquier
> objeto* de manera «profesional».

![Página maestra `Numerada derecha` con su nuevo marco de texto.]

### Adición y alineación de número de página

Para colocar el número de página a la página maestra `Normal derecha`, haz lo
siguiente:

1.  Da doble clic al marco de texto
2.  Selecciona `Insertar > Carácter > Número de página`[^5]

![Ubicación de `Número de página` dentro del menú principal.]

Con esto tenemos un número `3` dentro de la marco de texto. Este número es
generado automáticamente por [Scribus] y nos muestra el número `3` porque
estamos en la página maestra número 3. Este número cambiará según la página en
la que se encuentre.

La posibilidad de generación automática de la numeración de página permite
evitar errores en la numeración del documento. La ventaja de automatizar y
uniformar elementos de las páginas es una de las cuestiones fundamentales por
las que es muy conveniente el uso de páginas maestras.

Con el marco de texto seleccionado ahora se alinea el número de página de
manera correcta:

1.  Ve al panel derecho de `Propiedades de texto`[^6]
2.  Haz caso omiso a todas las opciones, **excepto**:
    -   Alineación horizontal: `Centrar texto` (segundo botón entre las
        opciones de interlineado e idioma)
    -   Selecciona lo siguiente en las opciones de
        `Columnas y distancias de textos`:
        -   Alineamiento vertical: `Inferior`

![Panel de `Propiedades de texto` con el marco de texto seleccionado.]

Por el momento no interesan las otras propiedades del texto, incluyendo el
idioma, ya que más adelante se modificarán mediante el uso de estilos de
párrafo.

Si todo se realizó de manera correcta, el número de página debería estar
centrado y en la parte inferior del marco de texto. A continuación se duplicará
esta página maestra para crear la página maestra `Normal izquierda`.

> **TIP**. El uso del panel `Propiedades de texto`, al igual que los paneles
> `Propiedades` y `Alinear y distribuir` permiten una gestión más «profesional»
> de los elementos. ¡No los desestimes!, quizá en el futuro requieras
> utilizarlos para tener un resultado más uniforme y exacto.

![Página maestra `Numerada derecha` con su número de página alineado.]

### Duplicación de una página maestra

La duplicación de una página maestra es una tarea muy sencilla y similar a la
[creación de una página maestra]. Para crear la página maestra
`Numerada izquierda` a continuación se duplica la página maestra
`Numerada derecha`:

1.  Ve al panel derecho de `Gestionar páginas maestras`[^7]
2.  Selecciona la página maestra `Numerada derecha`
3.  Da clic en el botón `Duplicar la página maestra elegida` (tercer botón del
    panel)
4.  Ingresa la siguiente información en la ventana flotante:
    -   Nombre: `Numerada izquierda`
    -   Disposición: `Página izquierda`
5.  Da clic en `Aceptar`

De esta manera ya se cuenta con dos páginas maestras numeradas, una para las
páginas derechas y otra para las izquierdas.

Para terminar la edición de las páginas maestras:

1.  Da clic en `Finalizar edición` (botón en la esquina inferior izquierda)

### Arreglo de un error de visualización

Al hacer clic en `Finalizar edición`, [Scribus] nos cierra la ventana de
edición de páginas maestras y nos muestra la ventana convencional en donde
están las páginas del documento.

Sin embargo, es frecuente encontrarnos con un error de visualización por el
cual [Scribus] no nos permite navegar a través de las páginas del documento.

![Error de visualización después de finalizar la edición de páginas maestras.]

Arreglar este error es muy sencillo:

1.  Guarda el documento desde `Archivo > Guardar`
2.  Da clic en `Ver > Zoom > Ajustar al alto`

### Aplicación de páginas maestras

Hasta este momento ya se cuenta con las páginas maestras necesarias:

-   Dos páginas maestras `Normal` para las páginas a la derecha y a la
    izquierda.
-   Dos páginas maestras `Numerada` para las páginas a la derecha y a la
    izquierda.

Sin embargo, en el documento las páginas aún se muestran sin numeración ya que
todas tienen aplicadas las páginas maestras `Normal`.

![Documento sin páginas numeradas]

Para aplicar las páginas maestras numeradas, haz lo siguiente:

1.  Da clic en `Página > Aplicar página maestra…`
2.  Ingresa la siguiente información en la ventana flotante:
    -   Página maestra: `Numerada derecha`
    -   Aplicar a: `Páginas impares`
3.  Da clic en `Aceptar`

![Ventana flotante de `Aplicar página maestra…`]

Con este cambio ya puedes ver las páginas derechas con la numeración correcta y
automática. Para numerar las páginas izquierdas repite el procedimiento pero
con la siguiente información en la ventana flotante:

-   Página maestra: `Numerada izquierda`
-   Aplicar a: `Páginas pares`

> **TIP**. Puede ser confuso hablar de páginas «derechas» e «izquierdas», así
> como de páginas «impares» y «pares». No obstante, la asociación es muy fácil
> para los idiomas que se leen de izquierda a derecha, como el español o el
> inglés: toda página derecha es impar y toda página izquierda es par. Si
> todavía tienes dificultades, consulta alguno de tus libros y ve la numeración
> que está a la derecha y a la izquierda.

![Documento con páginas numeradas]

Enhorabuena, haz completado el segundo ejercicio de este taller. Si tienes
dudas sobre el resultado final, puedes consultar el archivo de [este
ejercicio].

## Tarea

1.  Repite el ejercicio pero además añade una cornisa (texto al superior del
    margen) a las páginas numeradas con las siguientes características:
    -   Cada página maestra numerada debe tener diferente texto en la cornisa
    -   El texto debe estar centrado

## Enlaces de interés

-   [Archivo de este ejercicio][este ejercicio]
-   [Wiki de Scribus]
-   [Tutorial de Scribus]
-   [Manual de Scribus]
-   [Videos de Scribus][Videos en YouTube]
-   [Sitio de Scribus][Scribus]
-   [Sitio de Inkscape][Inkscape]
-   [Made with Scribus, publicaciones hechas con
    Scribus][una sección en inglés]
-   [Revistas Colimotl del Círculo Ometeotl, hechas con Scribus]
-   [*WikiEducación. Prácticas y experiencias educativas*, libro hecho con
    Scribus]
-   [Artículo de Wikipedia sobre Scribus]
-   [Artículo de Wikipedia sobre *desktop publishing* (en
    inglés)][*desktop publishing*]
-   [Artículo de Wikipedia sobre el *software libre*][*software* libre]
-   [Artículo de Wikipedia sobre TeX][TeX]

[^1]: Si por algún motivo no te muestra la ventana, ábrela desde
    `Archivo > Nuevo…`.

[^2]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Ordenar páginas`.

[^3]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Propiedades`.

[^4]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Alinear y distribuir`.

[^5]: Si por algún motivo la opción no está habilitada, regresa al paso previo.
    Es fácil saber cuando se ha hecho doble clic al marco de texto porque
    aparece una barra horizontal palpitante. Por otro lado, la opción
    `Número de páginas` (en plural) es para mostrar la cantidad total de
    páginas del documento.

[^6]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Content Properties`. El panel se encuentra nombrado como
    `Propiedades de texto`, una pequeña inconsistencia en la interfaz de
    [Scribus].

[^7]: Si por algún motivo no te muestra el panel, ábrelo desde
    `Ventanas > Ordenar páginas`. El nombre difiere porque en este punto del
    ejercicio se supone que te encuentras en el panel de edición de páginas
    maestras. Si este no es el caso, haz doble clic en alguna de las páginas
    maestras disponibles.

  [➡️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion2.md
  [Scribus]: https://www.scribus.net
  [Inkscape]: https://inkscape.org
  [TeX]: https://es.wikipedia.org/wiki/TeX
  [*desktop publishing*]: https://en.wikipedia.org/wiki/Desktop_publishing
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
  [Wiki de Scribus]: https://wiki.scribus.net/canvas/Pagina_principal
  [Tutorial de Scribus]: https://we.riseup.net/assets/115672/Tutorial-Scribus.pdf
  [Manual de Scribus]: https://web.archive.org/web/20230403225010/https://www.imh.eus/es/imh/comunicacion/docu-libre/manual-scribus
  [Videos en YouTube]: https://www.youtube.com/watch?v=gPUf15sbYL8&list=PLmJE_P_j3_IfTZ_AEUvfZXBSoZ0t7vHov
  [una sección en inglés]: https://www.scribus.net/category/made-with-scribus
  [1]: https://issuu.com/ram_saa/docs/colimotl01
  [2]: https://issuu.com/ram_saa/docs/colimotl02
  [3]: https://issuu.com/ram_saa/docs/colimotl03
  [4]: https://issuu.com/ram_saa/docs/colimotl04
  [Programando LIBREros]: https://gitlab.com/prolibreros
  [*WikiEducación. Prácticas y experiencias educativas*]: https://gitlab.com/prolibreros/cc/wikieducacion2022
  [Wikimedia México]: https://mx.wikimedia.org
  [Ventana de nuevo documento.]: ../imagenes/sesion1_01.jpg
  [Nuevo documento después de dar clic en `Aceptar`.]: ../imagenes/sesion1_02.jpg
  [Nuevo documento después de habilitar todas las opciones de `Marcos de texto`.]:
    ../imagenes/sesion1_03.jpg
  [Panel de `Ordenar páginas`.]: ../imagenes/sesion1_04.jpg
  [Ventana de edición de páginas maestras.]: ../imagenes/sesion1_05.jpg
  [Cuatro botones para gestionar páginas maestras.]: ../imagenes/sesion1_06.jpg
  [Ventana flotante al dar clic en `Añadir una nueva página maestra`.]: ../imagenes/sesion1_07.jpg
  [Antes y después de crear un `Marco de texto`.]: ../imagenes/sesion1_08.jpg
  [Panel de `Propiedades` con el marco de texto seleccionado.]: ../imagenes/sesion1_09.jpg
  [Panel de `Alinear y distribuir` con el marco de texto seleccionado.]: ../imagenes/sesion1_10.jpg
  [Página maestra `Numerada derecha` con su nuevo marco de texto.]: ../imagenes/sesion1_11.jpg
  [Ubicación de `Número de página` dentro del menú principal.]: ../imagenes/sesion1_12.jpg
  [Panel de `Propiedades de texto` con el marco de texto seleccionado.]: ../imagenes/sesion1_13.jpg
  [Página maestra `Numerada derecha` con su número de página alineado.]: ../imagenes/sesion1_14.jpg
  [creación de una página maestra]: #creación-de-una-página-maestra
  [Error de visualización después de finalizar la edición de páginas maestras.]:
    ../imagenes/sesion1_15.jpg
  [Documento sin páginas numeradas]: ../imagenes/sesion1_16.jpg
  [Ventana flotante de `Aplicar página maestra…`]: ../imagenes/sesion1_17.jpg
  [Documento con páginas numeradas]: ../imagenes/sesion1_18.jpg
  [este ejercicio]: https://gitlab.com/prolibreros/tt/ted-colima/-/raw/main/ejercicios/ejercicio1.sla?ref_type=heads&inline=false
  [Revistas Colimotl del Círculo Ometeotl, hechas con Scribus]: https://issuu.com/ram_saa
  [*WikiEducación. Prácticas y experiencias educativas*, libro hecho con Scribus]:
    https://gitlab.com/prolibreros/cc/wikieducacion2022/-/raw/no-masters/wikieducacion2022.digital.pdf
  [Artículo de Wikipedia sobre Scribus]: https://es.wikipedia.org/wiki/Scribus
