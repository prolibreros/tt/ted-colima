# [⬅️] Sesión 4 [➡️]

## Videos

-   [Tareas preliminares]
-   [Elaboración de estilo para negrita]
-   [Elaboración de estilo para itálica]
-   [Elaboración de estilo para negrita e itálica]
-   [Elaboración de estilo para versalita]
-   [Elaboración de estilo para epígrafe sencillo]
-   [Elaboración de estilo para epígrafe con múltiples párrafos]
-   [Elaboración de estilo para recuadro]
-   [Elaboración de estilo para lista no numerada]
-   [Elaboración de estilo para lista numerada]

  [⬅️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion3.md
  [➡️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion5.md
  [Tareas preliminares]: ../videos/sesion4_00-tareas_preliminares.webm
  [Elaboración de estilo para negrita]: ../videos/sesion4_01-elaboracion_de_estilo_para_negrita.webm
  [Elaboración de estilo para itálica]: ../videos/sesion4_02-elaboracion_de_estilo_para_italica.webm
  [Elaboración de estilo para negrita e itálica]: ../videos/sesion4_03-elaboracion_de_estilo_para_negrita_e_italica.webm
  [Elaboración de estilo para versalita]: ../videos/sesion4_04-elaboracion_de_estilo_para_versalita.webm
  [Elaboración de estilo para epígrafe sencillo]: ../videos/sesion4_05-elaboracion_de_estilo_para_epigrafe_sencillo.webm
  [Elaboración de estilo para epígrafe con múltiples párrafos]: ../videos/sesion4_06-elaboracion_de_estilo_para_epigrafe_con_multiples_parrafos.webm
  [Elaboración de estilo para recuadro]: ../videos/sesion4_07-elaboracion_de_estilo_para_recuadro.webm
  [Elaboración de estilo para lista no numerada]: ../videos/sesion4_08-elaboracion_de_estilo_para_lista_no_numerada.webm
  [Elaboración de estilo para lista numerada]: ../videos/sesion4_09-elaboracion_de_estilo_para_lista_numerada.webm
