# [⬅️] Sesión 5

## Videos

-   Tareas preliminares
-   Configuración para una nueva publicación
-   Cambio de dimensiones para una nueva publicación
-   Importación de contenido
-   Elaboración de estilo para notas al pie
-   Incorporación y gestión de notas al pie
-   Elaboración de estilo para pie de imagen
-   Incorporación y gestión de imágenes
-   Elaboración de tabla de contenidos
-   Revisiones clave al finalizar una nueva publicación
-   Exportación de una nueva publicación

  [⬅️]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion4.md
