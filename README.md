# TED Cultura Colima

Este es el repositorio del Taller de Edición Digital impartido a la
[Subsecretaría de Cultura del Estado de Colima] en 2023.

El taller fue planificado para un cupo máximo de 5 personas y con una duración
de 10 horas de clase y 20 horas adicionales para la elaboración de tareas.

## Sesiones

-   [Sesión 1]
-   [Sesión 2]
-   [Sesión 3]
-   [Sesión 4]
-   [Sesión 5]

## Requisitos

-   Computadora
-   Conexión a Internet
-   [Scribus] (de preferencia en su versión 1.7.0)

## Objetivo

El objetivo del taller consiste en la elaboración en PDF de un libro
previamente publicado por la subsecretaría. Para la elaboración del PDF se
utiliza [Scribus], un programa de maquetación con licencia de [*software*
libre].

El rediseño de una publicación tiene como motivo que el personal pueda comparar
los procesos y sus resultados, para después decidir la conveniencia de adoptar
este flujo de trabajo editorial en las próximas publicaciones de la
subsecretaría.

## Temario

1.  ¿Qué es Scribus? Consulta de materiales de apoyo
2.  Exploración de la interfaz de Scribus
3.  Configuración de un nuevo documento de Scribus
4.  Elaboración y aplicación de páginas maestras
5.  Diseño y aplicación de estilos de párrafo y de carácter
6.  Importación de documentos externos de Office Word o LibreOffice Writer
7.  Gestión de cajas de texto
8.  Importación y acomodo de imágenes
9.  Creación de tabla de contenidos
10. Exportación a PDF

  [Subsecretaría de Cultura del Estado de Colima]: https://www.col.gob.mx/cultura
  [Sesión 1]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion1.md
  [Sesión 2]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion2.md
  [Sesión 3]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion3.md
  [Sesión 4]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion4.md
  [Sesión 5]: https://gitlab.com/prolibreros/tt/ted-colima/-/blob/main/notas/sesion5.md
  [Scribus]: https://www.scribus.net/
  [*software* libre]: https://es.wikipedia.org/wiki/Software_libre
